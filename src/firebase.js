// firebase setup

import * as firebase from 'firebase'

const config = {
    apiKey: "AIzaSyCPRw17-M0kQRi8hAM1dq0Xw46V-_4nDPM",
    authDomain: "note-app-c0253.firebaseapp.com",
    databaseURL: "https://note-app-c0253.firebaseio.com",
    projectId: "note-app-c0253",
    storageBucket: "",
    messagingSenderId: "98999021710"
  }

  //firebase initialize
  export const firebaseApp = firebase.initializeApp(config)

  export const database = firebase.database().ref('/posts')

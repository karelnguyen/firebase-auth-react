import React, { Component } from 'react'
import { firebaseApp } from '../firebase'

class SignUp extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      error: {
        message: ''
      }
    }
  }

  signUp() {
    const { email, password } = this.state
    firebaseApp.auth().createUserWithEmailAndPassword(email, password)
      .catch(error => {
        this.setState({error})
        alert(error)
      })

  }

  render() {
    return (
      <div>
        <h2>Sign Up</h2>
        <input
          className='si-input-email'
          placeholder='email'
          onChange={e => this.setState({email: e.target.value})}
        />
        <input
          className='si-input-pw'
          type='password'
          placeholder='password'
          onChange={e => this.setState({password: e.target.value})}
        />
        <button
          className='si-btn'
          type='button'
          onClick={() => this.signUp()}
        >
          Sign up

        </button>
      </div>
    )
  }
}

export default SignUp

import React, {Component} from 'react'
import {firebaseApp} from '../firebase'

class Navbar extends Component {



  signOut() {
    firebaseApp.auth().signOut()
  }

  render() {
    return(
      <div>
        <button
          onClick = {() => this.signOut()}
        >Sign Out</button>
      </div>
    )
  }
}

export default Navbar

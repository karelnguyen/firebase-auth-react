import React, { Component } from 'react'
import {database} from '../firebase'
import _ from 'lodash'
import renderHTML from 'react-render-html'
import Navbar from './Navbar'



class Feed extends Component {
  constructor(props) {
    super(props)
    this.state = {
      posts: {}
    }
  }

  // lifecycle
  componentDidMount() {
    database.on('value', snapshot => {
      this.setState({
        posts: snapshot.val()
      })
    })
  }

  // render post from firebase
  renderPosts() {
    return _.map(this.state.posts, (post, key) => {
      return (
        <div key={key}>
          <h2>{post.title}</h2>
          <div>{renderHTML(post.body)}</div>
        </div>)
    })
  }

  render() {
    return(
      <div>
        <Navbar />
        {this.renderPosts()}
      </div>
    )
  }
}

export default Feed

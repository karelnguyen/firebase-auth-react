import React, { Component } from 'react'
import {database, firebaseApp} from '../firebase'
import ReactQuill from 'react-quill'
import 'react-quill/dist/quill.snow.css'
import renderHTML from 'react-render-html'
import Navbar from './Navbar'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      title: '',
      body: ''
    }
    // binding in constructor for better peformance (recommended)
    this.onHandleChange = this.onHandleChange.bind(this)
    this.onHandleSubmit = this.onHandleSubmit.bind(this)
  }

  componentDidMount() {
    firebaseApp.auth().onAuthStateChanged(user => {
      if (user) {
        console.log('SIGN IN OK', user)
        this.props.history.push('/feed')
      } else {
        console.log('NOT SIGNED IN')
        this.props.history.replace('/sign-in')
      }
    })
  }


  onHandleChange(e) {
    this.setState({body: e})
    console.log(e);
  }

  onHandleSubmit(e) {
    e.preventDefault()
    const post = {
      title: this.state.title,
      body: this.state.body
    }
    // push object post to firebase
    database.push(post)
    alert('posted motherfucker')
    this.setState({
      title:'',
      body:''
    })
  }

  render() {
    return (
      <div className="App">
        <Navbar />
        <form onSubmit={this.onHandleSubmit}>
          <input
            value={this.state.title}
            type='text'
            name='title'
            placeholder='title'
            onChange={e => {this.setState({title: e.target.value})}}
            ref='title'/>
          <ReactQuill
            modules={App.modules}
            formats={App.formats}
            value= {this.state.body}
            placeholder='body'
            onChange={this.onHandleChange}
          />
          <button>Post</button>
        </form>
        <br />
      </div>
    );
  }
}

App.modules = {
  toolbar: [
    [{'header': '1'}, {'header': '2'}, {'font': []}],
    [{'size': []}],
    ['bold', 'italic', 'underline', 'strike', 'blockquote'],
    [{'list': 'ordered'}, {'list': 'bullet'}],
    ['link', 'image', 'video'],['code-block'],
    ['clean']
  ]
}

App.formats = [
  'header', 'font', 'size', 'bold', 'italic', 'underline', 'strike', 'blockquote', 'list', 'bullet', 'link', 'image', 'video', 'code-block', 'clean'
]
export default App;

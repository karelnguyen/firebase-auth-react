import React, { Component } from 'react'
import { firebaseApp } from '../firebase'



class SignIn extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      error: {
        message: ''
      }
    }
  }

  componentDidMount() {
    firebaseApp.auth().onAuthStateChanged(user => {
      if (user) {
        console.log('SIGN IN OK', user)
        this.props.history.push('/feed')
      } else {
        console.log('NOT SIGNED IN')
        this.props.history.replace('/sign-in')
      }
    })
  }

  signIn() {
    const { email, password } = this.state
    firebaseApp.auth().signInWithEmailAndPassword(email, password)
      .catch(error => {
        this.setState({error})
        alert(error)
      })
  }

  render() {
    return (
      <div>
        <h2>Sign In</h2>
        <input
          className='si-input-email'
          placeholder='email'
          onChange={e => this.setState({email: e.target.value})}
        />
        <input
          className='si-input-pw'
          type='password'
          placeholder='password'
          onChange={e => this.setState({password: e.target.value})}
        />
        <button
          className='si-btn'
          type='button'
          onClick={() => this.signIn()}
        >
          Sign In
        </button>
      </div>
    )
  }
}

export default SignIn

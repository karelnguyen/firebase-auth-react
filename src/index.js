import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import Feed from './components/Feed'
import SignIn from './components/SignIn'
import SignUp from './components/SignUp'
import {firebaseApp} from './firebase'
import { BrowserRouter, Route, withRouter } from 'react-router-dom'

ReactDOM.render(
  <BrowserRouter >
    <div>
      <Route exact path='/app' component={App} />
      <Route exact path='/sign-in' component={SignIn} />
      <Route exact path='/sign-up' component={SignUp} />
      <Route exact path='/feed' component={Feed} />
    </div>
  </BrowserRouter>
  , document.getElementById('root')
);
